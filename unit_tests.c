#include "unity/src/unity.h"
#include <SDL2/SDL.h>
#include "graphics.h"
#include "display.h"

void test_fillGrid()
{
  int test_grid[48][64];
  fillGrid(test_grid);
  TEST_ASSERT_NOT_NULL(test_grid);
}

void setUp()
{

}

void tearDown()
{

}

int main()
{
    UNITY_BEGIN();
    RUN_TEST(test_fillGrid);

    return UNITY_END();
}
