#include "graphics.h"
#include "display.h"

SDL_Window* win;
SDL_Renderer* canvas;

int WINDOW_Y = GRID_SIZE_Y * (CELL_SIZE + 1) - 1;
int WINDOW_X = GRID_SIZE_X * (CELL_SIZE + 1) - 1;

int current_x = 0;
int current_y = 0;

int isRunning = 1;

int grid[GRID_SIZE_Y][GRID_SIZE_X];

void fillGrid(int g[GRID_SIZE_Y][GRID_SIZE_X])
{
  for(int y = 0; y < GRID_SIZE_Y; y++)
  {
    for (int x = 0; x < GRID_SIZE_X; x++)
    {
      g[y][x] = 0;
    }
  }
}

int save_file(FILE* file)
{
  if (fwrite(grid, sizeof(grid), 1, file)!=NULL)
    return 0;
  else
    return 1;
}

int load_file(FILE* file)
{
  if (fread(grid, sizeof(grid), 1, file)!=NULL)
    return 0;
  else
    return 1;
}

int load_SDL()
{
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    printf("Error initialising SDL: %s", SDL_GetError());
    return 1;
  }

  win = SDL_CreateWindow("Evolve", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_X, WINDOW_Y, 0);

  if (win == NULL)
  {
    printf("Error, window could not be created: %s", SDL_GetError());
    return 1;
  }

  canvas = SDL_CreateRenderer(win, -1, 0);
  isRunning = 1;
  return 0;
}

int currentMouseXY()
{
  SDL_PumpEvents();
  int x_int, y_int;
  int click = SDL_GetMouseState(&x_int, &y_int);
  double x = x_int;
  double y = y_int;
  x = x / (CELL_SIZE + 1);
  y = y / (CELL_SIZE + 1);
  current_x = (int) floor(x);
  current_y = (int) floor(y);
  return click;
}

void mouseClick()
{
  int btn = currentMouseXY();
  if (btn == 1)
  {
    grid[current_y][current_x] = 1;
  }
  if (btn == 4)
  {
    grid[current_y][current_x] = 0;
  }
}

void keyPress()
{
  int neighbour_count;
  int temp_arr[GRID_SIZE_Y][GRID_SIZE_X];
  fillGrid(temp_arr);

  for (int y = 0; y < GRID_SIZE_Y; y++)
  {
    for (int x = 0; x < GRID_SIZE_X; x++)
    {
        neighbour_count = 0;
        for (int y_adj = y - 1; y_adj < y + 2; y_adj++)
        {
          for (int x_adj = x - 1; x_adj < x + 2; x_adj++)
          {
            if (y_adj >= 0 && y_adj < GRID_SIZE_Y && x_adj >= 0 && x_adj < GRID_SIZE_X)
            {
              if (grid[y_adj][x_adj] == 1)
                neighbour_count++;
            }
          }
        }
        if (grid[y][x] == 1)
        {
          neighbour_count--; // Making sure you don't count the original cell itself
          if (neighbour_count == 2 || neighbour_count == 3)
            temp_arr[y][x] = 1;
          else
            temp_arr[y][x] = 0;
        }
        else
        {
          if (neighbour_count == 3)
            temp_arr[y][x] = 1;
          else
            temp_arr[y][x] = 0;
        }
    }
  }
  for (int y = 0; y < GRID_SIZE_Y; y++)
  {
    for (int x = 0; x < GRID_SIZE_X; x++)
    {
      grid[y][x] = temp_arr[y][x];
    }
  }
}

void display_SDL()
{
  SDL_SetRenderDrawColor(canvas, 154, 154, 154, 255);
  SDL_RenderClear(canvas);
  for (int y = 0; y < GRID_SIZE_Y; y++)
  {
    for (int x = 0; x < GRID_SIZE_X; x++)
    {
      SDL_Rect cell;
      cell.x = x * (CELL_SIZE + 1);
      cell.y = y * (CELL_SIZE + 1);
      cell.h = CELL_SIZE;
      cell.w = CELL_SIZE;
      if (grid[y][x] == 1)
        SDL_SetRenderDrawColor(canvas, 255, 154, 0, 255);
      else if (grid[y][x] == 0)
        SDL_SetRenderDrawColor(canvas, 100, 100, 100, 255);
      if (x == current_x && y == current_y)
        SDL_SetRenderDrawColor(canvas, 200, 200, 200, 255);
      SDL_RenderFillRect(canvas, &cell);
    }
  }
}

void loop()
{
  SDL_Event event;
  while (isRunning == 1)
  {
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
      {
        isRunning = 0;
      }
      else if (event.type == SDL_KEYDOWN)
      {
        keyPress();
      }
    }
    mouseClick();
    display_SDL();
    SDL_RenderPresent(canvas);
    SDL_Delay(1000/60);
  }
}

void quit()
{
  FILE *input = fopen("saves/autosave.bin", "w");
  save_file(input);
  fclose(input);
  SDL_DestroyRenderer(canvas);
  SDL_DestroyWindow(win);
  save_prompt();
  SDL_Quit();
}
