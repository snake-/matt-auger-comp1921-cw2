#include "display.h"

int ValidInt() // maybe put this into a utility.c because this would be useful throughout the program?
{
  char input[50];
  int output;
  scanf("%s", input);
  output = strtol(input, NULL, 10); // if input is not a number output will be 0
  return output;
}

void welcome_msg()
{
  printf("Welcome to: \n");
  printf(" _____           _           \n");
  printf("|  ___|         | |          \n");
  printf("| |____   _____ | |_   _____ \n");
  printf("|  __\\ \\ / / _ \\| \\ \\ / / _ \\\n");
  printf("| |___\\ V / (_) | |\\ V /  __/\n");
  printf("\\____/ \\_/ \\___/|_| \\_/ \\___|\n\n");
}

void main_menu_msg()
{
  printf("Please select an option: \n1.) Start new game\n2.) Load game\n3.) Exit\n");
}

void start_game_msg()
{
  printf("Welcome to Evolve. Left click to place live cells and right click to remove.\nTo begin evolution press any key.\n");
}

void load_game_msg()
{
  printf("Previous game state succesfully loaded.\n");
}

void save_prompt()
{
  char input;
  printf("Would you like to save the current game state? [y/n]:\n");
  scanf(" %c", &input);
  if (input == 'y')
  {
    char save_name[20];
    char file_name[30];
    printf("Please enter save name: \n");
    scanf("%s", save_name);
    sprintf(file_name, "saves/%s.bin", save_name);
    FILE* input = fopen(file_name, "w");
    save_file(input);
  }
}

int directory_list()
{
    struct dirent *de;
    DIR *dr = opendir("saves");

    if (dr == NULL)
    {
        printf("Could not open current directory" );
        return 0;
    }
    printf("\nPlease select a game save:\n");
    while ((de = readdir(dr)) != NULL)
      if (de->d_name[0] != '.')
        printf("%s\n", de->d_name);
    closedir(dr);
    return 0;
}

char* load_prompt()
{

}


int options()
{
  main_menu_msg();
  int input = ValidInt();
  if (input == 1)
  {
    start_game_msg();
    return 1;
    // call graphics.c loop
  }
  else if (input == 2)
  {
    load_game_msg();
    start_game_msg();
    return 2;
  }
  else if (input == 3)
  {
    printf("Goodbye!\n");
    return 3;
  }
  else
  {
    printf("Error, an invalid selection has been made\n");
    return 0;
  }
}
