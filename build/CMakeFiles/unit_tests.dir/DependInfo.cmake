# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/csunix/sc20mra/matt-auger-comp1921-cw2/display.c" "/home/csunix/sc20mra/matt-auger-comp1921-cw2/build/CMakeFiles/unit_tests.dir/display.c.o"
  "/home/csunix/sc20mra/matt-auger-comp1921-cw2/graphics.c" "/home/csunix/sc20mra/matt-auger-comp1921-cw2/build/CMakeFiles/unit_tests.dir/graphics.c.o"
  "/home/csunix/sc20mra/matt-auger-comp1921-cw2/unit_tests.c" "/home/csunix/sc20mra/matt-auger-comp1921-cw2/build/CMakeFiles/unit_tests.dir/unit_tests.c.o"
  "/home/csunix/sc20mra/matt-auger-comp1921-cw2/unity/src/unity.c" "/home/csunix/sc20mra/matt-auger-comp1921-cw2/build/CMakeFiles/unit_tests.dir/unity/src/unity.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/SDL2"
  "../unity/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
