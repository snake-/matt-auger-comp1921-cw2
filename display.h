#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int ValidInt();

void welcome_msg();

void main_menu_msg();

void start_game_msg();

void save_prompt();

int options();

#endif
