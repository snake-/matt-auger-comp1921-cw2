#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL_mouse.h>

#define GRID_SIZE_X (64) // change this to a variable in future if you want custom grid sizes
#define GRID_SIZE_Y (48)
#define CELL_SIZE (8)

extern int grid[GRID_SIZE_Y][GRID_SIZE_X];

void fillGrid(int g[GRID_SIZE_Y][GRID_SIZE_X]);

int save_file(FILE* file);

int load_file(FILE* file);

int load_SDL();

int currentMouseXY();

void mouseClick();

void keyPress();

void display_SDL();

void loop();

void savePrompt();

void quit();


#endif
