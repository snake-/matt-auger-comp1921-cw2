#include "graphics.h"
#include "display.h"

int main()
{
  int isRunning = 1;
  welcome_msg();
  while (isRunning == 1)
  {
    int selection = options();
    if (selection == 1)
    {
      fillGrid(grid);
      load_SDL();
      loop();
      quit();
    }
    else if (selection == 2)
    {
      char file[20];
      directory_list();
      scanf("%s", file);
      char file_location[30];
      sprintf(file_location, "saves/%s.bin", file);
      FILE *input = fopen(file_location, "r");
      if (input != NULL)
      {
        load_SDL();
        load_file(input);
        loop();
        quit();
      }
      else
      {
        printf("Error, please select a valid file\n");
      }
    }
    else if (selection == 3)
    {
      isRunning = 0;
    }
  }

  return 0;
}
